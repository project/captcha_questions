README FILE FOR THE CAPTCHA QUESTIONS MODULE FOR DRUPAL
-------------------------------------------------------

CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * REQUIREMENTS
 * INSTALLATION
 * CONFIGURATION
 * USING THE CAPTCHA
 * KNOWN UNKNOWNS
 * FUTURE DEVELOPMENT
 * CONTACT

INTRODUCTION
------------

 - Captcha questions is a light-weight module that employs a very simple
   captcha-mechanism to stop unwanted form submissions.

REQUIREMENTS
------------

 - No dependencies.

INSTALLATION
------------

 - Extract the files in your module directory (typically /modules)
 - Visit the modules page and enable the module
 - From the modules page you will find links to
    - permission settings
    - configuration
    - help

CONFIGURATION
------------

1. Enable the module using drush or by going to `admin/modules`.
2. Go to `/admin/config/people/captcha_questions` for configurations related to
   captcha questions module.

USING THE CAPTCHA
-----------------

 - You create a question/answer-pair and attach this to selected forms.
 - If the user fails to answer the question correctly, the form will not be
   submitted.
 - Logging to database is handled in a submodule and is entirely optional.
 - It is similar in nature to Trick Question and Captcha Riddler, but will be
   actively maintained.
 - Image captchas can be hard also for humans. Captcha questions was made after
   realising most spam can be blocked by a simple question that is easy for
   humans but very difficult for automated scripts.
 - It is recommended to make the questions as simple as possible.


KNOWN UNKNOWNS
--------------

Captcha questions has not been thoroughly tested and never
on multilingual sites.

FUTURE DEVELOPMENT
------------------

 - Set different question/answer pairs on different forms
 - Rotation between several question/answer pairs in one form
 - Write tests
 - Test on various site configurations, including multilingual sites
 - Translations

CONTACT
-------

Please provide feedback to agnar@norweb.no if you have problems, comments,
patches, praise or ideas.
